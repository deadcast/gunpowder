public class MySceneSpec : tsg_Gunpowder {
  protected override void Awake() {
    LoadSpecs = delegate() {
      describe("my game tests", delegate() {
        beforeEach(delegate() {
          loadScene("your scene name here");
        });
      
        describe("when the game starts", delegate() {
          it("should not show my game object", delegate() {
            expect(find("my game object name")).toBeHidden();
          });
        });
      });
    };
  }
}