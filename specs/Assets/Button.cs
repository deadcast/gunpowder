using UnityEngine;
using System.Collections;

public class Button : MonoBehaviour {
  public string content { get { return m_content; } }
  
  private string m_content = string.Empty;
  
  void OnGUI() {
    GUI.Button(new Rect(0, 0, 128, 32), m_content);
    
    if(tsg_Gunpowder.Input.KeyboardEvent("c", this.GetHashCode()))
      m_content += "Pressed C";
  }
}
