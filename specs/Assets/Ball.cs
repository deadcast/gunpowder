using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {
  void Update() {
    if(tsg_Gunpowder.Input.GetButton("Fire1")) {
      renderer.enabled = false;
    }
    
    if(tsg_Gunpowder.Input.GetButtonUp("Fire2", this.GetHashCode()+1)) {
      transform.position += new Vector3(5, 5, 5);
    }
  }

  void OnMouseEnter() {
    renderer.material.color = Color.green;
  }

  void OnMouseExit() {
    renderer.material.color = Color.yellow;
  }
}
