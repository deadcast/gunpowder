using UnityEngine;
using System.Collections;

// This exists so that checks can be made against null in an object-oriented approach
public class tsg_Null {
  public tsg_Null() {}
  
  public override bool Equals(object obj) {
    return this.GetType() == obj.GetType();
  }
  
  public override int GetHashCode() {
    return base.GetHashCode();
  }
}