using UnityEngine;
using System;
using System.Collections;

public class tsg_Simulate {
  public void move(string direction, float milliseconds, Func<Void> callback) {
    tsg_Gunpowder.simulationsToRun.Enqueue(new tsg_Simulation(direction, milliseconds, callback));
  }

  public void buttonPress(string button, float milliseconds, Func<Void> callback) {
    tsg_Gunpowder.simulationsToRun.Enqueue(new tsg_Simulation(button, milliseconds, callback));
  }

  public void keyPress(string key, float milliseconds, Func<Void> callback) {
    tsg_Gunpowder.simulationsToRun.Enqueue(new tsg_Simulation(key, milliseconds, callback));
  }
  
  public void waits(float milliseconds, Func<Void> callback) {
    tsg_Gunpowder.simulationsToRun.Enqueue(new tsg_Simulation(null, milliseconds, callback));
  }
}
