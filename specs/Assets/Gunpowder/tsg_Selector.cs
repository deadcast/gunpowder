using UnityEngine;
using System.Collections;

public class tsg_Selector {
  public GameObject gameObject { get { return m_gameObject; } }
  
  private GameObject m_gameObject;
 
  public tsg_Selector find(string selector) {
    m_gameObject = GameObject.Find(selector);
    return this;
  }

  public tsg_Selector moveTo(float x, float y, float z) {
    m_gameObject.transform.position = new Vector3(x, y, z);
    return this;
  }
  
  public tsg_Selector mouseEnter(string scriptName) {
    m_gameObject.GetComponent(scriptName).SendMessage("OnMouseEnter");
    return this;
  }
  
  public tsg_Selector mouseExit(string scriptName) {
    m_gameObject.GetComponent(scriptName).SendMessage("OnMouseExit");
    return this;
  }
}
