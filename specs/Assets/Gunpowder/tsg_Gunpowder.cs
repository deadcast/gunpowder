using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class tsg_Gunpowder : MonoBehaviour {

  // State information for specs
  public static bool failedExpectation = false;
  public static tsg_Simulation currentSimulation = null;
  public static string currentSpecContext;
  public static Queue<tsg_Simulation> simulationsToRun = new Queue<tsg_Simulation>();
  public static Stack<string> failedExpectations = new Stack<string>();
  public static tsg_Wrapper Input = new tsg_Wrapper();
  
  private tsg_Simulate simulate = new tsg_Simulate();
  private tsg_Selector selector = new tsg_Selector();
  
  static Queue<tsg_Spec> specsToRun = new Queue<tsg_Spec>();
  static Stack<Func<Void>> beforesToRun = new Stack<Func<Void>>();
  static Stack<Func<Void>> aftersToRun = new Stack<Func<Void>>();
  static Stack<string> specMessageContext = new Stack<string>();
  static int specCount = 0;
  static bool specsFinished = false;
  static string sceneName;
  
  protected static int failCount = 0;
  protected static int pendingCount = 0;
  protected static Func<Void> LoadSpecs = null;
 
  public tsg_Selector find(string objectName) { 
    return selector.find(objectName); 
  }

  // Simulate methods
  public void move(string direction, float milliseconds, Func<Void> callback) {
    simulate.move(direction, milliseconds, callback);
  }

  public void pressButton(string button, float milliseconds, Func<Void> callback) {
    simulate.buttonPress(button, milliseconds, callback);
  }

  public void pressKey(string key, float milliseconds, Func<Void> callback) {
    simulate.keyPress(key, milliseconds, callback);
  }

  public void waits(float milliseconds, Func<Void> callback) {
    simulate.waits(milliseconds, callback);
  }

  // Spec DSL
  public void loadScene(string name) { 
    sceneName = name; 
  }
 
  public void beforeEach(Func<Void> func) { 
    beforesToRun.Push(func); 
  }
 
  public void afterEach(Func<Void> func) {
    aftersToRun.Push(func);
  }
 
  public void context(string name, Func<Void> func) { 
    describe(name, func); // Alias 
  }
 
  public void describe(string name, Func<Void> func) {
    specMessageContext.Push(name);
    runTestPreparatorsAround(func);
    specMessageContext.Pop();
  }
 
  public tsg_Matcher expect(object actual) { 
    return new tsg_Matcher(actual);
  }
 
  public tsg_Matcher match(object actual) {
    return new tsg_Matcher(actual, false); 
  }
 
  public void xit(string name, Func<Void> func) { 
    it(name); // Alias 
  }
 
  public void it(string name) { 
    it(name, delegate(){});
    pendingCount++;
  }
 
  public void it(string name, Func<Void> func) {
    specsToRun.Enqueue(new tsg_Spec(
     new Stack<Func<Void>>(beforesToRun), 
     new Stack<Func<Void>>(aftersToRun), 
     func, 
     buildContextString(name)
   ));
  }

  protected virtual void Awake() {
  }
   
  void Start() {
    StartCoroutine(StartSpecs());
  }
 
  IEnumerator StartSpecs() {
    if(!specsFinished) {
      if(specsToRun.Count == 0) {
        beginSpecs();
      } else {
        yield return StartCoroutine(runNextSpec());
        updateSpecResults();
        resetScene();
      }
    } else {
      Debug.Log(specResults());
    }
  }
  
  IEnumerator runNextSpec() {
    if(specsToRun.Count == 1) {
      specsFinished = true;
    }
   
    tsg_Spec nextSpec = specsToRun.Dequeue();
   
    foreach(var before in nextSpec.befores) {
      before();
    }
   
    currentSpecContext = nextSpec.context;
    nextSpec.spec();
    yield return StartCoroutine(runSimulations());
   
    foreach(var after in nextSpec.afters) {
      after();
    }
   
    while(failedExpectations.Count != 0) {
      var error = failedExpectations.Pop();
      Debug.LogError(error);
    }
  }
 
  IEnumerator runSimulations() {
    while(simulationsToRun.Count != 0) {
      currentSimulation = simulationsToRun.Dequeue();
      yield return new WaitForSeconds(currentSimulation.milliseconds/1000.0f);
      currentSimulation.callback();
      currentSimulation = null;
      Input.ResetPressList();
    }
  }
 
  private void resetScene() {
    Application.LoadLevel(sceneName);
  }
 
  private void beginSpecs() {
    Debug.Log("Running Gunpowder specs...");
    LoadSpecs();
    specCount += specsToRun.Count;
    Start();
  }
 
  private void updateSpecResults() {
    if(failedExpectation) { 
      failCount++;
      failedExpectation = false;
    }
  }
 
  private string buildContextString(string name) {
    var context = string.Empty;
    for(var index = 0; index < specMessageContext.Count; index++) {
      context += (specMessageContext.Reverse().ToArray()[index] + " > "); //todo
    }
    return context + name;
  }
 
  private void runTestPreparatorsAround(Func<Void> func) {
    int beforeDescribeBeforesLength = beforesToRun.Count;
    int beforeDescribeAftersLength = aftersToRun.Count;
   
    func();
   
    int afterDescribeBeforesLength = beforesToRun.Count;
    int afterDescribeAftersLength = aftersToRun.Count;
   
    for(int index = 0; index < (afterDescribeBeforesLength - beforeDescribeBeforesLength); index++) {
      beforesToRun.Pop(); 
    }
   
    for(var index = 0; index < (afterDescribeAftersLength - beforeDescribeAftersLength); index++) { 
      aftersToRun.Pop(); 
    }
  }
 
  protected string specResults() {
    var passOrFail = failCount > 0 ? "FAIL: " : "PASS: ";
    var output = passOrFail + specCount + " specs, " + failCount + " failures, "
     + pendingCount + " pending, " + Time.time + " secs.";
    return output;
  }
}
