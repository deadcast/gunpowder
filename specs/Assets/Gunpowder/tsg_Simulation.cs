using UnityEngine;
using System;
using System.Collections;

public class tsg_Simulation {
  public string name;
  public float milliseconds;
  public Func<Void> callback;
 
  public tsg_Simulation(string name, float milliseconds, Func<Void> callback) {
    this.name = name;
    this.milliseconds = milliseconds;
    this.callback = callback;
  }
}
