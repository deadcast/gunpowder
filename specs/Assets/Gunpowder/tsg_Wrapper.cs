using UnityEngine;
using System.Collections.Generic;

public class tsg_Wrapper {
  private Dictionary<int, tsg_Null> m_pressList = new Dictionary<int, tsg_Null>();
  
  public void ResetPressList() {
    m_pressList.Clear();
  }

  public float GetAxis(string axis) {
    float speed = 0.0f;

    if(tsg_Gunpowder.currentSimulation != null) {
      
      var direction = tsg_Gunpowder.currentSimulation.name;
      
      if(direction == "forward" && axis == "Vertical") {
        speed = 0.1f;
      } else if(direction == "backward" && axis == "Vertical") {
        speed = -0.1f;
      } else if(direction == "left" && axis == "Horizontal") {
        speed = -0.1f;
      } else if(direction == "right" && axis == "Horizontal") {
        speed = 0.1f;
      }

      return speed;
    } else {
      return Input.GetAxis(axis);
    }
  }

  public bool GetButton(string buttonName) {
    if(tsg_Gunpowder.currentSimulation != null) {
      var button = tsg_Gunpowder.currentSimulation.name;
      return button == buttonName ? true : false;
    } else {
      return Input.GetButton(buttonName);
    }
  }
  
  public bool GetButtonUp(string buttonName, int uniquIdentifier) {
    if(tsg_Gunpowder.currentSimulation != null) {
      var button = tsg_Gunpowder.currentSimulation.name;
      if(button == buttonName && !m_pressList.ContainsKey(uniquIdentifier))
      {
        m_pressList.Add(uniquIdentifier, null);
        return true;
      }
      return false;
    } else {
      return Input.GetButtonUp(buttonName);
    }
  }

  public bool GetKey(string keyName) {
    if(tsg_Gunpowder.currentSimulation != null) {
      var key = tsg_Gunpowder.currentSimulation.name;
      return key == keyName ? true : false;
    } else {
      return Input.GetKey(keyName);
    }
  }
  
  public bool GetKeyUp(string keyName, int uniquIdentifier) {
    if(tsg_Gunpowder.currentSimulation != null) {
      var key = tsg_Gunpowder.currentSimulation.name;
      if(key == keyName && !m_pressList.ContainsKey(uniquIdentifier))
      {
        m_pressList.Add(uniquIdentifier, null);
        return true;
      }
      return false;
    } else {
      return Input.GetKeyUp(keyName);
    }
  }
  
  public bool KeyboardEvent(string keyName, int uniquIdentifier) {
    if(tsg_Gunpowder.currentSimulation != null) {
      var key = tsg_Gunpowder.currentSimulation.name;
      if(key == keyName && !m_pressList.ContainsKey(uniquIdentifier))
      {
        m_pressList.Add(uniquIdentifier, null);
        return true;
      }
      return false;
    } else {
      return Event.current.Equals(Event.KeyboardEvent(keyName));;
    }
  }
}
