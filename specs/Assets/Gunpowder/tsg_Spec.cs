using UnityEngine;
using System;
using System.Collections.Generic;

public class tsg_Spec {
  public Func<Void> spec;
  public Stack<Func<Void>> befores;
  public Stack<Func<Void>> afters;
  public string context;
 
  public tsg_Spec(Stack<Func<Void>> befores, Stack<Func<Void>> afters, Func<Void> spec, string context) {
    this.context = context;
    this.befores = befores;
    this.afters = afters;
    this.spec = spec;
  }
}
