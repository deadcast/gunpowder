using UnityEngine;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class tsg_Matcher {
  private object m_actual;
  private bool m_negate;
  private bool m_failed;
  private bool m_showErrors;
 
  public tsg_Matcher(object actual, bool options = true) {
    initialize(actual, options);
  }

  void initialize(object actual, bool options) {
    m_negate = false;
    m_failed = false;
    m_showErrors = options;

    if(actual == null) {
      m_actual = new tsg_Null();
    } else {
      switch(actual.GetType().ToString()) {
      case "tsg_Selector":
        m_actual = (actual as tsg_Selector).gameObject;
        break;
      case "tsg_Matcher":
        m_failed = (actual as tsg_Matcher).m_failed;
        break;
      default:
        m_actual = actual;
        break;
      }
    }
  }

  void failed(string message) {
    if(m_showErrors) {
      tsg_Gunpowder.failedExpectation = true;
      tsg_Gunpowder.failedExpectations.Push(tsg_Gunpowder.currentSpecContext + " > " + message);
    }
    m_failed = true;
  }

  public tsg_Matcher toExist() {
    var actualExists = (m_actual != null && m_actual.GetType() != typeof(tsg_Null));

    if(m_negate) {
      if(actualExists) {
        failed("expected " + m_actual + " not to exist");
      }
    } else {
      if(!actualExists) {
        failed("expected " + m_actual + " to exist");
      }
    }

    return this;
  }
  
  public tsg_Matcher toMatch(string pattern) {
    if(m_negate) {
      if(Regex.IsMatch(m_actual.ToString(), pattern)) {
        failed("expected " + m_actual + " not to match pattern");
      }
    } else {
      if(!Regex.IsMatch(m_actual.ToString(), pattern)) {
        failed("expected " + m_actual + " to match pattern");
      }
    }

    return this;
  }

  public tsg_Matcher toBeTruthy() {
    if(m_negate) {
      if(m_actual.Equals(true)) {
        failed("expected " + m_actual + " not to be truthy");
      }
    } else {
      if(!m_actual.Equals(true)) {
        failed("expected " + m_actual + " to be truthy");
      }
    }

    return this;
  }

  public tsg_Matcher toBeFalsy() {
    if(m_negate) {
      if(m_actual.Equals(false)) {
        failed("expected " + m_actual + " not to be falsy");
      }
    } else {
      if(!m_actual.Equals(false)) {
        failed("expected " + m_actual + " to be falsy");
      }
    }

    return this;
  }

  public tsg_Matcher toEqual(object expected) {
    bool result = false;
    expected = expected == null ? new tsg_Null() : expected;
    
    if(m_actual.GetType().IsArray && expected.GetType().IsArray) {
      object[] aryActual = ((IEnumerable)m_actual).Cast<object>().ToArray();
      object[] aryExpected = ((IEnumerable)expected).Cast<object>().ToArray();
      result = Enumerable.SequenceEqual(aryActual, aryExpected);
    } else {
      result = m_actual.Equals(expected);
    }
    
    if(m_negate) {
      if(result) {
        failed("expected " + m_actual + " not to equal " + expected);
      }
    } else {
      if(!result) {
        failed("expected " + m_actual + " to equal " + expected);
      }
    }
    
    return this;
  }

  public tsg_Matcher toHavePosition(float expectedX, float expectedY, float expectedZ) {
    return toHavePosition(expectedX, expectedY, expectedZ, 0.0f);
  }

  public tsg_Matcher toHavePosition(float expectedX, float expectedY, float expectedZ, float within) {
    var actualPosition = ((GameObject)m_actual).transform.position;
    var expectedPosition = new Vector3(expectedX, expectedY, expectedZ);

    if(m_negate) {
      if(Vector3.Distance(actualPosition, expectedPosition) <= within) {
        failed("expected " + ((GameObject)m_actual).name + " not to have position " + expectedPosition + " but got " + actualPosition);
      }
    } else {
      if(Vector3.Distance(actualPosition, expectedPosition) > within) {
        failed("expected " + ((GameObject)m_actual).name + " to have position " + expectedPosition + " but got " + actualPosition);
      } 
    }

    return this;
  }

  public tsg_Matcher toBeVisible() {
    if(m_negate) {
      if(((GameObject)m_actual).renderer.enabled) {
        failed("expected " + ((GameObject)m_actual).name + " not to be visible");
      }
    } else {
      if(!((GameObject)m_actual).renderer.enabled) {
        failed("expected " + ((GameObject)m_actual).name + " to be visible");
      }
    }

    return this;
  }

  public tsg_Matcher toBeHidden() {
    if(m_negate) {
      if(!((GameObject)m_actual).renderer.enabled) {
        failed("expected " + ((GameObject)m_actual).name + " not to be hidden");
      }
    } else {
      if(((GameObject)m_actual).renderer.enabled) {
        failed("expected " + ((GameObject)m_actual).name + " to be hidden");
      }
    }

    return this;
  }
  
  public tsg_Matcher toHaveColor(Color color) {
    if(m_negate) {
      if(((GameObject)m_actual).renderer.material.color == color) {
        failed("expected " + ((GameObject)m_actual).name + " not to have color " + color);
      }
    } else {
      if(((GameObject)m_actual).renderer.material.color != color) {
        failed("expected " + ((GameObject)m_actual).name + " to have color " + color);
      }
    }

    return this;
  }

  public tsg_Matcher not {
    get {
      m_negate = true;
      return this;
    }
  }

  public void toPass() {
    if(m_failed) {
      failed("expected to pass but failed");
    }
  }

  public void toFail() {
    if(!m_failed) {
      failed("expected to fail but passed");
    }
  }
}