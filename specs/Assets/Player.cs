using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
  void Update() {
    var inputValueX = tsg_Gunpowder.Input.GetAxis("Vertical");
    var inputValueZ = tsg_Gunpowder.Input.GetAxis("Horizontal");
    transform.Translate(new Vector3(inputValueX, 0, inputValueZ));
  }
}
