using UnityEngine;
using System.Text.RegularExpressions;
using System.Collections;

public class SpecSuite : tsg_Gunpowder {
  protected override void Awake() {
  
    LoadSpecs = delegate() {
      
      describe("gunpowder", delegate() {
        beforeEach(delegate() {
          loadScene("test");
        });
       
        describe("test results", delegate() {
          describe("passing specs", delegate() {
            it("passes", delegate() {
              expect(1).toEqual(1);
              expect(failedExpectations.Count).toEqual(0);
            });
           
            it("shows the right output", delegate() {
              var match = Regex.Match(specResults(), "PASS: (.+) specs, 0 failures, (.+) pending, (.+) secs.");
              expect(match.Success).toBeTruthy();
            });
          });
         
          describe("failing specs", delegate() {
            it("fails", delegate() {
              expect(5).toEqual(1);
              expect(failedExpectations.Pop()).toEqual("gunpowder > test results > failing specs > fails > expected 5 to equal 1");
            });
           
            context("another failure", delegate() {
              it("fails again", delegate() {
                expect(true).toBeFalsy(); 
                expect(failedExpectations.Pop()).toEqual("gunpowder > test results > failing specs > another failure > fails again > expected True to be falsy");
              });
            });
           
            it("shows the right output", delegate() {
              var match = Regex.Match(specResults(), "FAIL: (.+) specs, 2 failures, (.+) pending, (.+) secs.");
              expect(match.Success).toBeTruthy();
              failCount -= 2; // Hide these test errors from the real test results output
            });
          });
         
          describe("pending specs", delegate() {
            it("will test something");
 
            xit("is failing", delegate() {
              expect(1).toEqual(5);
            });
           
            it("shows the right output", delegate() {
              var match = Regex.Match(specResults(), "PASS: (.+) specs, 0 failures, 2 pending, (.+) secs.");
              expect(match.Success).toBeTruthy();
              pendingCount -= 2; // Hide these pending tests from the real test results output
            });
          });
        });
   
        describe("matchers", delegate() {
          describe("toMatch", delegate() {
            it("is match", delegate() {
              expect(match("testing is good").toMatch(@"testing is [a-z]+")).toPass();
              expect(match("testing is 123").toMatch(@"testing is [a-z]+")).toFail();
            });
 
            it("is not match", delegate() {
              expect(match("testing is 123").not.toMatch(@"testing is [a-z]+")).toPass();
              expect(match("testing is good").not.toMatch(@"testing is [a-z]+")).toFail();
            });
          });
          
          describe("toBeTruthy", delegate() {
            it("is true", delegate() {
              expect(match(true).toBeTruthy()).toPass();
              expect(match(false).toBeTruthy()).toFail();
            });
 
            it("is not true", delegate() {
              expect(match(false).not.toBeTruthy()).toPass();
              expect(match(true).not.toBeTruthy()).toFail();
            });
          });
 
          describe("toBeFalsy", delegate() {
            it("is false", delegate() {
              expect(match(false).toBeFalsy()).toPass();
              expect(match(true).toBeFalsy()).toFail();
            });
 
            it("is not false", delegate() {
              expect(match(true).not.toBeFalsy()).toPass();
              expect(match(false).not.toBeFalsy()).toFail();
            });
          });
 
          describe("toExist", delegate() {
            it("is null", delegate() {
              expect(match(null).not.toExist()).toPass();
              expect(match(null).toExist()).toFail();
            });
 
            it("exists in scene", delegate() {
              expect(match(find("box")).toExist()).toPass();
              expect(match(find("box")).not.toExist()).toFail();
            });
          });
 
          describe("toEqual", delegate() {
            it("equals", delegate() {
              expect(match(1).toEqual(1)).toPass();
              expect(match(1).toEqual(3)).toFail();
            });
 
            it("does not equal", delegate() {
              expect(match(3).not.toEqual(1)).toPass();
              expect(match(1).not.toEqual(1)).toFail();
            });
            
            context("arrays", delegate() {
              it("equals", delegate() {
                expect(match(new int[] {1, 3, 5}).toEqual(new int[] {1, 3, 5})).toPass();
                expect(match(new int[] {5, 3, 8}).toEqual(new int[] {1, 3, 5})).toFail();
              });
              
              it("does not equal", delegate() {
                expect(match(new int[] {1, 3, 5}).not.toEqual(new int[] {2, 2, 2})).toPass();
                expect(match(new int[] {1, 3, 5}).not.toEqual(new int[] {1, 3, 5})).toFail();
              });
            });
            
            context("null", delegate() {
              it("equals", delegate() {
                expect(match(null).toEqual(null)).toPass();
                expect(match(null).toEqual("string")).toFail(); 
              });
              
              it("does not equal", delegate() {
                expect(match(null).not.toEqual("string")).toPass();
                expect(match(null).not.toEqual(null)).toFail(); 
              });
            });
          });
 
          describe("toHavePosition", delegate() {
            it("has the correct position", delegate() {
              find("box").gameObject.transform.position = new Vector3(1, 2, 3);
              expect(match(find("box")).toHavePosition(1, 2, 3)).toPass();
              expect(match(find("box")).toHavePosition(3, 2, 1)).toFail();
            });
 
            it("does not have the correct position", delegate() {
              find("box").gameObject.transform.position = new Vector3(1, 2, 3);
              expect(match(find("box")).not.toHavePosition(3, 2, 1)).toPass();
              expect(match(find("box")).not.toHavePosition(1, 2, 3)).toFail();
            });
          });
 
          describe("toBeVisible", delegate() {
            it("is visible", delegate() {
              find("ball").gameObject.renderer.enabled = true;
              expect(match(find("ball")).toBeVisible()).toPass();
 
              find("ball").gameObject.renderer.enabled = false;
              expect(match(find("ball")).toBeVisible()).toFail();
            });
 
            it("is not visible", delegate() {
              find("ball").gameObject.renderer.enabled = false;
              expect(match(find("ball")).not.toBeVisible()).toPass();
 
              find("ball").gameObject.renderer.enabled = true;
              expect(match(find("ball")).not.toBeVisible()).toFail();
            });
          });
 
          describe("toBeHidden", delegate() {
            it("is hidden", delegate() {
              find("box").gameObject.renderer.enabled = false;
              expect(match(find("box")).toBeHidden()).toPass();
 
              find("box").gameObject.renderer.enabled = true;
              expect(match(find("box")).toBeHidden()).toFail();
            });
 
            it("is not hidden", delegate() {
              find("box").gameObject.renderer.enabled = true;
              expect(match(find("box")).not.toBeHidden()).toPass();
 
              find("box").gameObject.renderer.enabled = false;
              expect(match(find("box")).not.toBeHidden()).toFail();
            });
          });
         
          describe("toHaveColor", delegate() {
            it("has the correct color", delegate() {
              var box = find("box");
              box.gameObject.renderer.material.color = Color.red;
              expect(match(box).toHaveColor(Color.red)).toPass();
            });
           
            it("does not have the correct color", delegate() {
              var box = find("box");
              box.gameObject.renderer.material.color = Color.red;
              expect(match(box).toHaveColor(Color.blue)).toFail();
            });
          });
        });
 
        describe("helpers", delegate() {
          describe("find", delegate() {
            it("can find the game object", delegate() {
              expect(match(find("box")).toEqual(GameObject.Find("box"))).toPass();
              expect(match(find("box")).toEqual(GameObject.Find("ball"))).toFail();
            });
          });
 
          describe("move", delegate() {
            it("moves forward", delegate() {
              move("forward", 100, delegate() {
                expect(find("player")).toHavePosition(2.1f, 0.5f, -13.0f, 0.5f);
              });
            });
 
            it("moves backwards", delegate() {
              move("backward", 100, delegate() {
                expect(find("player")).toHavePosition(1.1f, 0.5f, -13.0f, 0.5f);
              });
            });
 
            it("moves right", delegate() {
              move("right", 100, delegate() {
                expect(find("player")).toHavePosition(1.6f, 0.5f, -12.5f, 0.5f);
              });
            });
 
            it("moves left", delegate() {
              move("left", 100, delegate() {
                expect(find("player")).toHavePosition(1.6f, 0.5f, -13.5f, 0.5f);
              });
            });
 
            it("can run multiple movements in a single spec", delegate() {
              move("backward", 100, delegate() {
                expect(find("player")).toHavePosition(1.1f, 0.5f, -13.0f, 0.8f);
              });
 
              move("left", 100, delegate() {
                expect(find("player")).toHavePosition(1.1f, 0.5f, -14.0f, 0.8f);
              });
            });
          });
 
          describe("pressButton", delegate() {
            it("triggers GetButton events", delegate() {
              expect(find("ball")).toBeVisible();
              pressButton("Fire1", 500, delegate() {
                expect(find("ball")).toBeHidden();
              });
            });
            
            it("triggers GetButtonUp events", delegate() {
              find("ball").moveTo(0, 0, 0);
              pressButton("Fire2", 200, delegate() {
                expect(find("ball")).toHavePosition(5.0f, 5.0f, 5.0f); 
              });
              pressButton("Fire2", 200, delegate() {
                expect(find("ball")).toHavePosition(10.0f, 10.0f, 10.0f); 
              });
            });
          });
 
          describe("pressKey", delegate() {
            it("triggers GetKey events", delegate() {
              expect(find("box")).toBeVisible();
              pressKey("h", 500, delegate() {
                expect(find("box")).toBeHidden();
              });
            });
            
            it("triggers GetKeyUp events", delegate() {
              find("box").moveTo(0, 0, 0);
              pressKey("q", 200, delegate() {
                expect(find("box")).toHavePosition(5.0f, 5.0f, 5.0f); 
              });
              pressKey("q", 200, delegate() {
                expect(find("box")).toHavePosition(10.0f, 10.0f, 10.0f); 
              }); 
            });
            
            it("triggers current keyboard events", delegate() {
              pressKey("c", 200, delegate() {
                var content = find("GUI").gameObject.GetComponent<Button>().content;
                expect(content.Length).not.toEqual(0);
              }); 
            });
          });
 
          describe("moveTo", delegate() {
            it("moves the object to the correct location", delegate() {
              find("ball").moveTo(0, 3, 2);
              expect(find("ball").gameObject.transform.position.x).toEqual(0.0f);
              expect(find("ball").gameObject.transform.position.y).toEqual(3.0f);
              expect(find("ball").gameObject.transform.position.z).toEqual(2.0f);
            });
          });
 
          describe("mouseEnter", delegate() {
            it("triggers the OnMouseEnter on the object", delegate() {
              var ball = find("ball");
              ball.mouseEnter("Ball");
              expect(ball).toHaveColor(Color.green);    
            });
          });
         
          describe("mouseExit", delegate() {
            it("triggers the OnMouseExit on the object", delegate() {
              var ball = find("ball");
              ball.mouseExit("Ball");
              expect(ball).toHaveColor(Color.yellow);    
            });
          });
        });
 
        describe("resetting the scene", delegate() {
          context("a spec finishes", delegate() {
            var boxPosition = find("box").gameObject.transform.position;
            var ballPosition = find("ball").gameObject.transform.position;
 
            it("moves the objects", delegate() {
              find("box").moveTo(5, 5, 5);
              find("ball").moveTo(3, 3, 3);
            });
 
            it("resets the objects", delegate() {
              expect(find("box")).toHavePosition(boxPosition.x, boxPosition.y, boxPosition.z);
              expect(find("ball")).toHavePosition(ballPosition.x, ballPosition.y, ballPosition.z);
            });
          });
        });
 
        describe("beforeEach", delegate() {
          var number = 0;
          beforeEach(delegate() {
            number++; });
 
          it("increments the number", delegate() {
            expect(number).toEqual(1);
          });
 
          describe("nested beforeEach", delegate() {
            beforeEach(delegate() {
              number--; });
 
            it("increments and decrements the number", delegate() {
              expect(number).toEqual(1);
            });
          });
 
          it("increments the number again", delegate() {
            expect(number).toEqual(2);
          });
         
          it("increments the number yet again", delegate() {
            expect(number).toEqual(3);
          });
        });
       
        describe("afterEach", delegate() {
          var number = 3;
          afterEach(delegate() {
            number--; });
 
          it("does not change the number", delegate() {
            expect(number).toEqual(3);
          });
 
          describe("nested afterEach", delegate() {
            afterEach(delegate() {
              number++; });
 
            it("decrements the number", delegate() {
              expect(number).toEqual(2);
            });
           
            it("increments and decrements the number", delegate() {
              expect(number).toEqual(2);
            });
          });
 
          it("decrements the number again", delegate() {
            expect(number).toEqual(2);
          });
         
          it("decrements the number yet again", delegate() {
            expect(number).toEqual(1);
          });
        });
      });
      
      describe("waits", delegate() {
        it("waits the specified time then runs the callback", delegate() {
          find("cylinder").gameObject.AddComponent(typeof(Rigidbody));
          expect(find("plane")).toBeVisible();
          waits(300, delegate() {
            expect(find("plane")).toBeHidden();
          });
        });
      });
      
    };
    
  }
}
        
